import { Component } from "@angular/core";
import {
  NavController,
  LoadingController,
  ToastController,
} from "@ionic/angular";

@Component({
  selector: "app-home",
  templateUrl: "home.page.html",
  styleUrls: ["home.page.scss"],
})
export class HomePage {
  constructor(
    public navCtrl: NavController,
    public loadingCtrl: LoadingController,
    public toastCtrl: ToastController
  ) {}

  principal() {
    this.navCtrl.navigateRoot("/principal");
    this.message_start();
  }

  async message_start() {
    const loading = await this.loadingCtrl.create({
      message: "Cargando Calculadora",
      duration: 2000,
    });
    loading.present();

    const toast = await this.toastCtrl.create({
      message: "Bienvenido(a)",
      duration: 2000,
      cssClass: "toast-message",
    });
    toast.present();
  }
}
